package assignmentCollectiondemo;

import java.util.Scanner;

public class PrimeNum {

	public static void main(String[] args) {
		
	       Scanner s = new Scanner(System.in);  
	       System.out.print("Enter a number : ");  // user input to enter a number to check prime or not
	       int num = s.nextInt(); 
	       
	       if (isPrime(num)) {  // if block gets executed if isPrime methods is true
	           System.out.println(num + " is a prime number");  
	       } else {  
	           System.out.println(num + " is not a prime number");// else block gets executed if isPrime methods is false
	       }  
	   }  
	  
	   public static boolean isPrime(int num) {  
	       if (num <= 1) {  // if number is less than 1 then it not a prime number
	           return false;  
	       }  
	       
	       for (int i = 2; i < num; i++) {  
	    	   int temp=num%i;  // using temp variable to store remainder
	           if (temp== 0) {  // if remainder is 0 then it is not prime 
	               return false;  
	           }  
	       }  
	       return true;  // else it is prime number
	   }
	}

