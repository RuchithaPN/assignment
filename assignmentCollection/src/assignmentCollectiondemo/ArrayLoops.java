package assignmentCollectiondemo;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayLoops {

	public static void main(String[] args) {
		
	        ArrayList<String> list = new ArrayList<String>();
	            list.add("50");
	            list.add("40");
	            list.add("45");
	            System.out.println("Size of the array" +list.size());// to print the size of the array
	            System.out.println("While Loop :"); // while loop  with iterator
	            
	            
	            Iterator<String> demo = list.iterator();
	            while(demo.hasNext())  // has next element is used to display all elements
	            {
	                System.out.println(demo.next());
	            }
	            
	            // Advance for loop
	            System.out.println("Advanced Loop:");
	            for(Object obj : list)   // object creation 
	            {
	                System.out.println(obj); // prints the object
	            }
	            
	            //For loop
	            System.out.println("For Loop :");
	            for(int index=0; index<list.size(); index++)   //for loop to traverse the array elements from 0 index 
	            {
	                
	                System.out.println(list.get(index)); // prints all elements using get(index)
	            }
	        }
	    }
