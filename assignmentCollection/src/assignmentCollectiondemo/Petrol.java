package assignmentCollectiondemo;
import java.util.Scanner;
	
	public class Petrol {

      public static void main(String[] args) {
			   
	        Scanner sc = new Scanner(System.in);
	        System.out.print("Enter the type of vehicle bike or car : ");
	        String vehicle = sc.nextLine();
	        System.out.print("Enter the amount of petrol: ");
	        int amount = sc.nextInt();

	       result(amount,vehicle);
	        }
      
	        public static void result(int amount, String vehicle) {
	        	
	        float litres = amount/115;
	        float distance=0;
	        double miles;
	        
	        if(vehicle.equalsIgnoreCase("car"))
	        {
	        distance = litres*8;
	        miles=distance/1.6;
	        }
	        else if(vehicle.equalsIgnoreCase("bike"))
	        {
	        distance = litres*20;
	        miles=distance/1.6;
	        }
	        else
	        {
	        System.out.println("Enter valid vehicle type - (bike/car)");
	        return;
	        }
	        System.out.println("Litres of petrol customer gets : " + litres + "liters");
	        System.out.println("Distance customer can travel (km & miles): "+distance+ "km"+" "+miles+"miles");
	        }
	        }