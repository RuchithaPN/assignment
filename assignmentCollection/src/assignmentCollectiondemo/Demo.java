package assignmentCollectiondemo;
import java.util.Arrays;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the numbers:");
        int arr[] = new int[5];
        int sum = 0;
        
        for(int i=0;i<5;i++)
        {
            arr[i] = sc.nextInt();
            sum=sum+ arr[i];
        }
        Arrays.sort(arr);
        int a = arr[4];
        int b = arr[0];
        int minSum = sum-a;
        int maxSum = sum-b;
        System.out.println(minSum + " is the minimum sum\n" +" The maximum sum is " + maxSum);
                               
   }

}
