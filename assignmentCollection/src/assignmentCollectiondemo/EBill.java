package assignmentCollectiondemo;

import java.util.*;  

//create class ElectricityBillExample4 to calculate electricity bill using Inheritance  
class EBill extends CalculateBill  
{   
 // main() method start  
 public static void main(String args[])   
 {     
     // declare variable units  
     int units,age;  

     // create Scanner class object to take input from user  
     Scanner sc = new Scanner(System.in);  

     System.out.println("Enter number of units for calculating electricity bill.");  
     units = sc.nextInt(); 
     
     System.out.println("Enter the age of the customers:");  
     age = sc.nextInt(); 

     System.out.println("The electricity bill for "+units+" is:" + getBill(units));   
 }   
}  
    
//create simple class CalculateBill  
class CalculateBill  
{     
 // variable to calculate electricity bill to pay  
 static double billToPay;  
    
 static double getBill(long units)  
 {  
     // check whether units are less than 100  
     if(units < 100)  
     {  
         billToPay = units*0;  
     }  
     // check whether the units are less than 300  
     else if(units>101 && units <= 200){  
         billToPay = units*10;  
     }  
     // check whether the units are greater than 300  
     else if(units >201 && units< 400)  
     {  
         billToPay = units*25;  
     } 
     else if(units >401 && units< 1000)  
     {  
         billToPay = units*50;  
     }
     else if(units >100)  
     {  
    	  
    	  billToPay = 500000;  
    	  System.out.println("Pay Penality: " + billToPay);
     }
     return billToPay;    
 }
}
