package assignmentCollectiondemo;

import java.util.Scanner;

public class Ticket {

	public static void main(String[] args) {
		
		    Scanner sc = new Scanner(System.in);
		    System.out.println("Enter the number of ticket:"); //user input to enter the number of tickets
		    int ticket = sc.nextInt();
		    
		    if(ticket>=5 && ticket<=40)
		    {
		        System.out.println("Do you need refreshment:");  // enter y for yes and n for no 
		        String refresh = sc.next();
		        
		        System.out.println("Do you have coupon code:");  //enter y for yes and n for no 
		        String cupcode = sc.next();
		        
		        System.out.println("Enter the circle:");  // enter k for king class ticket and q for queen class ticket
		        String circle = sc.next();
		        
		        double amt = 0;
		        switch(circle)
		        {
		            case "k":
		                {
		                    int price = 75; // price of king ticket is 75
		                    amt = (ticket*price);
		                    
		                    if(ticket>20)  // discount for booking more than 20 tickets with 10 discount
		                    {
		                        amt = amt - ((amt*10)/100);
		                    }
		                    if(cupcode.equals("y")) // condition for coupon with 20% discount
		                    {
		                        amt = amt - ((amt*20)/100);
		                    }
		                    if(refresh.equals("y"))
		                    {
		                        amt = amt + (50*ticket);
		                    }
		                    
		                    System.out.println("Total cost:"+ amt);
		                    break;
		                }
		                
		            case "q":
		                {
		                    int price = 150;  // price of queen ticket is 150
		                    amt = ticket*price;
		                    if(ticket>20)
		                    {
		                        amt = amt -((amt*10)/100);//discount for booking more than 20 tickets with 10% discount
		                    }
		                    if(cupcode.equals("y"))
		                    {
		                        amt = amt -((amt*20/100));// condition for coupon with 20% discount
		                    }
		                    if(refresh.equals("y"))
		                    {
		                        amt = amt+(50*ticket);
		                    }
		                    
		                    System.out.println("Total cost:"+ amt);
		                    break;
		                }
		            default:
		                System.out.println("Invalid Input... Please enter the valid input..!!!");
		        }
		    }
		    else
		    {
		        System.out.println("Invalid ...!!! Number of tickets should not be less than 5 and greater than 40....");
		    }
		}
	
	}

