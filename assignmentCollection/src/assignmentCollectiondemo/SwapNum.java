package assignmentCollectiondemo;

import java.util.Scanner;

public class SwapNum {

	public static void main(String[] args) {
			  
		        Scanner sc=new Scanner(System.in);
				double a, b, temp;
				
				System.out.print(" Enter the First Number : ");
				a = sc.nextDouble();	
				
				System.out.print(" Enter the Second Number : ");
				b = sc.nextDouble();	
				
				System.out.println("\n Before Swapping : a =  " + a + " and b =  " + b);
				
				temp = a;  // Value of first number is assigned to temporary variable
				a = b;   // Value of second number is assigned to first
				b = temp;  // Value of temporary variable is assigned to second
				
				System.out.println("\n After  Swapping:  a =  " + a + " and b =  " + b);
			}
}

