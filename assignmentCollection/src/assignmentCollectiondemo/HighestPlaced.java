package assignmentCollectiondemo;

import java.util.Scanner;

public class HighestPlaced {

	public static void main(String[] args) { 
		
		
		        Scanner sc=new Scanner(System.in); //user input to enter the number of students
		        System.out.println("Enter the number of students placed in CSE:");
		            int CSE= sc.nextInt();
		            
		        System.out.println("Enter the number of students placed in ECE:");
		            int ECE=sc.nextInt();
		            
		        System.out.println("Enter the number of students placed in MECH:");
		            int MECH=sc.nextInt();
		            
		        if(CSE<0||ECE<0||MECH<0)    // condition if input is negative (less than 0)
		        {
		            System.out.println("Input is Invalid");
		        }
		        
		        else if(CSE==ECE && ECE==MECH)  // condition if all the departments have equal number of placed students
		        {
		            System.out.println("None of the department has got the highest placement");
		        }
		        
		        else if(CSE==ECE && MECH<CSE)
		        {
		            System.out.println("Highest placed branch");
		            System.out.println("CSE");
		            System.out.println("ECE");
		        }
		        
		        else if(CSE==MECH && ECE<MECH)
		        {
		            System.out.println("Highest placed branch");
		            System.out.println("CSE");
		            System.out.println("MECH");
		        }
		        
		        else if(ECE==MECH && CSE<MECH)
		        {
		            System.out.println("Highest placed branch");
		            System.out.println("ECE");
		            System.out.println("MECH");
		        }
		        
		        
		        else if(CSE>ECE && CSE>MECH)
		        {
		            System.out.println("Highest placed branch");
		            System.out.println("CSE");
		        }
		        
		        
		        else if(ECE>CSE && ECE>MECH)
		        {
		            System.out.println("Highest placed branch");
		            System.out.println("ECE");
		        }
		        
		        
		        else if(MECH>CSE && MECH>ECE)
		        {
		            System.out.println("Highest placed branch");
		            System.out.println("MECH");
		        }
		    }
		}


