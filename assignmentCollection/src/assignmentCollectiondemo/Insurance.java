package assignmentCollectiondemo;

import java.util.*;

public class Insurance {

		    double calculateAverage(int[] age) // creating the method to calculate the average
		    {
		        int n=age.length; // gives the total number of elements in the array
		        double sum=0.0;
		        
		        for(int i=0;i<n;i++)
		        {
		            sum=sum+age[i];
		        }
		        
		        double average=sum/n;
		        return average;
		    }
		    
		    public static void main (String[] args) {
		    	
		        Scanner sc =new Scanner(System.in);
		        Insurance ins=new Insurance();
		        System.out.println("Enter total number of employees:");  //  user input to enter the number of employees
		        int num=sc.nextInt();
		        
		        int flag=0;
		        if(num>1) {  // if condition to check that the given number of employees is greater than 1 else gives invalid
		        
		        	int[] empage=new int[num];
		            System.out.println("Enter the age for "+num+" employees:");
		            
		            for(int i=0;i<num;i++)
		            {
		                int demo=sc.nextInt();
		                
		                if(demo>=28 && demo<=40) // condition to check whether the age is between 28 & 40
		                {
		                    empage[i]=demo;
		                }
		                else
		                {
		                    System.out.println("Invalid age... Please enter the age between 28 and 40 years... !!!");
		                    flag++;
		                    break;
		                }
		            }
		            if(flag==0)
		            {   
		                System.out.println("The average age is "+ins.calculateAverage(empage));
		            }
		        }
		        else
		        {
		            System.out.println("Please enter a valid employee count...!!!");
		        }
		    }
	}

