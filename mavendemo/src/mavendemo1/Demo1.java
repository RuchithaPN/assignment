package mavendemo1;

public class Demo1 {

		private String name; // three fields or variables.
		private int age;
		private String city;
		private String country;
		private String gender;
		
		//getters and setters method
		public String getName() {
		return name;
		}
		public void setName(String name) {
		this.name = name;
		}
		public int getAge() {
		return age;
		}
		public void setAge(int age) {
		this.age = age;
		}
		public String getCity() {
		return city;
		}
		public void setCity(String city) {
		this.city = city;
		}
		public String getCountry() {
		return city;
		}
		public void setCountry(String country) {
		this.country = country;
		}
		public String getGender() {
		return gender;
		}
		public void setGender(String gender) {
		this.gender = gender;
		}
		@Override
		public String toString() {
		return "Details [name=" + name + ", age=" + age + ", city=" + city + ", country=" + country +", gender" + gender +"]";
		}
	}
