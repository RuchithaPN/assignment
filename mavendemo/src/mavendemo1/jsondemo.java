package mavendemo1;


import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class jsondemo {
		// writing json
		
		public static void main(String[] args) {

		//Gson gson= new Gson();
			
		Gson gson= new GsonBuilder().setPrettyPrinting().create();
		Demo1 dd = createmyObject();
		System.out.println(dd); // To print java object printed
		String j = gson.toJson(dd); // to convert java object to JSON
		System.out.println(j);


		try(FileWriter writer = new FileWriter("C:\\Users\\241352\\Java-Workspace\\javademo.json")){
		gson.toJson(dd, writer);
		}
		catch(IOException e) {
		e.printStackTrace();
		}
		}
		private static Demo1 createmyObject() {
		Demo1 dd= new Demo1();
		dd.setName("Lucky");
		dd.setAge(25);
		dd.setCity("Banglore");
		dd.setCountry("India");
		dd.setGender("Female");
		return dd;
		}
	}

